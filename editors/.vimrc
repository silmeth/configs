" Turn off old vi compatibility
set nocompatible

" Turn on syntax highlighting
syntax on

" Turn off startup message
set shortmess+=I

" Show line numbers
set number
set relativenumber " Only current lno is absolute, the rest relative

" Always show the bottom status line even if only one window
set laststatus=2

" “Saner backspace” whatever that is
" set backspace=indent,eol,start

" Allow unsaved modified hidden buffers
set hidden

" Turn search case-insensitive, but only if all chars
" in the search string are lower-case
" set ignorecase
" set smartcase

" Incremental search (instead of return-committed)
" set incsearch

" Unmap Q as a shortcut for ex mode
nmap Q <nop>

" Disable error bell
set noerrorbells visualbell t_vb=

" Mouse support
" set mouse +=a

"Set space as the leader
let g:mapleader = "\<space>"

set termguicolors

" indentation
set expandtab
set tabstop=4
" 0 means the same as tabstop
set softtabstop=0
" 0 means the same as tabstop
set shiftwidth=0
set autoindent
set smartindent
set shiftround
" keep some context above and below the cursor
set scrolloff=8
" show trailing spaces and tabs
set list
set listchars=tab:→\ ,trail:·

" shorter timeout for mappings
set timeoutlen=200
set ttimeoutlen=30

" close buffer (and go to previous) without closing current window
nnoremap <silent> <leader>d :bprevious <bar> bdelete #<cr>
" easier returning to normal in terminal mode
tnoremap <esc><esc> <c-\><c-n>

augroup FileTypesSpecialization
    autocmd!
    autocmd FileType make setlocal noexpandtab
    autocmd FileType json,yaml setlocal tabstop=2
augroup END

