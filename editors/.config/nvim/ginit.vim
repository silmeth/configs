set mouse=a

if exists(':GuiFont')
    GuiFont! Iosevka Term Slab:h10
    GuiLinespace -1
endif

if exists(':GuiRenderLigatures')
    GuiRenderLigatures 1
endif
