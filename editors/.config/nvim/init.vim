set runtimepath^=~/.vim runtimepath+=~/.vim/after
" fzf has problems when &shell=/path/to/nu, so pretend we’re always on zsh
set shell=zsh
let &packpath = &runtimepath
source ~/.vimrc

" Plugins will be downloaded under the specified directory.
call plug#begin('~/.local/share/nvim/site/plugged')

" Declare the list of plugins.
" Language Server Protocol support
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neovim/nvim-lspconfig'
" for automated installation of LSP servers
" install servers with eg. `:LspInstall rust`
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
" code completions using nvim LSP or other sources
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-calc'
Plug 'hrsh7th/nvim-cmp'
" Snippets from language servers
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'
" treesitter-based syntax highlighting
" install languages with eg. `:TSInstall kotlin`, `:TSInstall rust`, etc.
Plug 'nvim-treesitter/nvim-treesitter' , { 'do': ':TSUpdate' }
" color theme
Plug 'morhetz/gruvbox'
" markdown-type tables editing
Plug 'dhruvasagar/vim-table-mode'
" Firefox integration
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
" Plug 'udalov/kotlin-vim' " Kotlin syntax highlighting (using treesitter instead now)
" fuzzy-finding – file navigations, ripgrep, etc.
Plug 'junegunn/fzf', { 'do': { _ -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" cargo integration for Rust
Plug 'rust-lang/rust.vim'
" mediawiki syntax
Plug 'chikamichi/mediawiki.vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" More space for displaying messages.
set cmdheight=2
" Default 4 s, shorter is better. Time of inactivity between swap-file saves
set updatetime=300

" LSP config
lua << EOF
local lspconfig = require'lspconfig'
local configs = require'lspconfig.configs'

local on_attach = function(client, bufnr)
    local opts = { remap=false, silent=true, buffer=bufnr }
    local function buf_set_keymap(mode, lhs, rhs) vim.keymap.set(mode, lhs, rhs, opts) end
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
    local function next_diagnostic(...) vim.diagnostic.jump { count=1, float=true, ... } end
    local function prev_diagnostic(...) vim.diagnostic.prev { count=-1, float=true, ... } end

    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Key mappings
    -- code navigation
    buf_set_keymap('n', 'gD', vim.lsp.buf.declaration)
    buf_set_keymap('n', 'gd', vim.lsp.buf.definition)
    buf_set_keymap('n', 'gi', vim.lsp.buf.implementation)
    buf_set_keymap('n', 'gr', vim.lsp.buf.references)
    buf_set_keymap('n', '[g', prev_diagnostic)
    buf_set_keymap('n', ']g', next_diagnostic)

    -- documentation and help
    buf_set_keymap('n', 'K', vim.lsp.buf.hover)
    buf_set_keymap('n', '<C-k>', vim.lsp.buf.signature_help)

    -- refactoring
    buf_set_keymap('n', '<leader>rn', vim.lsp.buf.rename)
    buf_set_keymap('n', '<leader>rf', function() vim.lsp.buf.format { async = true } end)
    buf_set_keymap('x', '<leader>rsf', function() vim.lsp.buf.format { async = true } end)
    buf_set_keymap('n', '<leader>ca', vim.lsp.buf.code_action)
end

local function setup_servers()
    -- enable snippets from Language Servers
    local capabilities = require('cmp_nvim_lsp').default_capabilities()

    require'mason'.setup()
    require'mason-lspconfig'.setup {
        ensure_installed = { "vimls", "lua_ls", "jdtls", "kotlin_language_server" }
    }
    local function config()
        return {
            on_attach = on_attach,
            capabilities = capabilities,
        }
    end

    local rust_config = config()
    rust_config.settings = {
        ['rust-analyzer'] = { check = { command = 'clippy' } }
    }
    lspconfig.rust_analyzer.setup(rust_config)

    local util = require('lspconfig/util')
    local kotlin_config = config()
    kotlin_config.settings = { kotlin = { compiler = { jvm = { target = '17' } } } }
    kotlin_config.init_options = { storagePath = util.path.join(vim.env.XDG_DATA_HOME, "nvim-data") }

    lspconfig.kotlin_language_server.setup(kotlin_config)
    lspconfig.jdtls.setup(config())
    lspconfig.lua_ls.setup(config())
    lspconfig.vimls.setup(config())
    lspconfig.ocamllsp.setup(config())
end

setup_servers()
EOF

" set gruvbox as the colorscheme
colorscheme gruvbox
" but make background transparent (use the terminal bg color)
if &termguicolors
     highlight Normal guibg=NONE
endif

" treesitter config
lua << EOF
require'nvim-treesitter.configs'.setup {
    ensure_installed = {
        'rust', 'kotlin', 'json', 'toml', 'java', 'comment',
        -- and these are installed by default by neovim package, need to override
        'bash', 'c', 'lua', 'markdown', 'markdown_inline', 'python', 'query', 'vim', 'vimdoc'
    },

    highlight = { enable = true },
}
vim.api.nvim_set_hl(0, "@text.todo", { link = "Todo" })
vim.api.nvim_set_hl(0, "@text.note", { link = "Todo" })
vim.api.nvim_set_hl(0, "@text.warning", { link = "WarningMsg" })
vim.api.nvim_set_hl(0, "@text.danger", { link = "ErrorMsg" })
EOF

lua << EOF
local cmp = require'cmp'
local luasnip = require'luasnip'

-- local has_words_before = function()
--     local line, col = unpack(vim.api.nvim_win_get_cursor(0))
--     return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match('%s') == nil
-- end

cmp.setup({
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    mapping = {
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ['<CR>'] = cmp.mapping.confirm({ select = false }),
        ['<C-n>'] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }), { 'i', 's' }),
        ['<C-p>'] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }), { 'i', 's' }),
        ['<C-b>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-e>'] = cmp.mapping(cmp.mapping.abort(), { 'i', 's', 'c' }),
        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            -- elseif has_words_before() then
            --     cmp.complete()
            else
                fallback()
            end
        end, { 'i', 's'}),
        ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end, { 'i', 's'}),
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'buffer', keyword_length = 4 },
        { name = 'calc' },
    })
})

-- cmdline autocompletion does not play nice with firenvim cmdline
if not vim.g.started_by_firenvim then
    -- use buffer source for '/' searching
    cmp.setup.cmdline('/', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
            { name = 'buffer' },
        })
    })

    -- use buffer and path for ':' command line
    cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
            { name = 'path' },
            { name = 'cmdline' },
        })
    })
end
EOF

" Firefox specific config
if exists('g:started_by_firenvim')
    let g:firenvim_config = { 
        \ 'globalSettings': {
            \ 'alt': 'all',
        \  },
        \ 'localSettings': {
            \ '.*': {
                \ 'priority': 0,
                \ 'takeover': 'never',
                \ 'cmdline': 'firenvim',
            \ },
        \ }
    \ }

    let &g:guifont = "Iosevka Term Slab:h15"

    augroup FirenVimFileTypes
        autocmd!
        autocmd BufEnter *duolingo.com_*.txt set filetype=markdown
        autocmd BufEnter play.kotlinlang.org_* set filetype=kotlin
        autocmd BufEnter *celtic-languages.org_* set filetype=mediawiki
        autocmd BufEnter *wikipedia.org_* set filetype=mediawiki
        autocmd BufEnter *wikisource.org_* set filetype=mediawiki
        autocmd BufEnter *wiktionary.org_* set filetype=mediawiki
    augroup END
endif

augroup Gradle
    " stolen from https://github.com/hsanson/vim-android/blob/master/autoload/efm.vim#L52-L79
    function! s:efm()
        let efm='%-G:%.%#,'                          " Filter out everything that starts with :
        let efm.='%t: %f:%l:%c,'                     " multiline Kotlin
        let efm.='%-GNote:%.%#,'                     " Filter out everything that starts with Note:
        let efm.='%-G  warning:%.%#,'
        let efm.='%t: file://%f:%l:%c: %m,'          " Gradle syntax/type error
        let efm.='%Wwarning: %m,'
        let efm.='findbugs: %tarning %f:%l:%c %m,'   " Find bugs
        let efm.='pmd: %tarning %f:%l:%c %m,'        " PMD
        let efm.='checkstyle: %tarning %f:%l:%c %m,' " Checkstyle
        let efm.='lint: %tarning %f:%l:%c %m,'       " Linter
        let efm.='%f:%l:%c: %trror: %m,'             " single aapt
        let efm.='%f:%l:%c: %tarning: %m,'           " single aapt
        let efm.='%EFAILURE: %m,'                    " Catch all exception start
        let efm.='%-ZBUILD FAILED,'                  " Catch all exception end
        let efm.='%Ee: %f:%l: error: %m,'
        let efm.='%Ww: %f:%l: warning: %m,'
        let efm.='%E%f:%l: error: %m,'
        let efm.='%W%f:%l: warning: %m,'
        let efm.='%W%f:%l:%c-%.%# Warning: %m,'      " multi manifest start
        let efm.='%E%f:%l:%c-%.%# Error: %m,'        " multi manifest start
        let efm.='%C%.%#%p^,'
        let efm.='%+Ie:  %.%#,'
        let efm.='%+Iw:  %.%#,'
        let efm.='%+I  %.%#,'
        let efm.='%t: %f:%l:%c %m,'                  " single Kotlin
        let efm.='%t: %f: (%l\, %c): %m,'            " single Kotlin
        let efm.='%t: %f: %m,'                       " single Kotlin
        let efm.='%f:%l:%c %m,'                      " ktlint
        let efm.='%-G%.%#'                           " Remove not matching messages
        return efm
    endfunction

    autocmd!
    autocmd FileType kotlin,java let &l:errorformat = s:efm() |
        \ setlocal makeprg=./gradlew
augroup END
