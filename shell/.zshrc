fpath=(~/.zfunc $fpath)
# The following lines were added by compinstall
zstyle :compinstall filename '/home/silmeth/.zshrc'

setopt extendedglob

autoload -Uz compinit

# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt inc_append_history autocd
setopt histignorespace
setopt histignorealldups
bindkey -e
# End of lines configured by zsh-newuser-install

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

setopt printExitValue

autoload -U zcalc

export PS1="%n@%m:%~%# "
export EDITOR="nvim"

export PATH="$HOME/bin:$HOME/.cargo/bin:$PATH"

#if [ -f ~/.config/exercism/exercism_completion.zsh ]; then
#  source ~/.config/exercism/exercism_completion.zsh
#fi

# OPAM configuration
# . /home/silmeth/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

compinit
